;(function(window, $) {

  var Video = function(elementRef, name) {
    this.elementRef = elementRef;
    this.name = name;
    this.trackStops = 10;
    this.lastStop = -1;
    this.attachEvents();

  }

  Video.prototype.attachEvents = function() {
    this.elementRef.addEventListener("play", this.handlePlayEvent(this));
    this.elementRef.addEventListener("ended", this.handleEndedEvent);
    this.elementRef.addEventListener("timeupdate", this.handleTimeUpdateEvent(this));
  }

  Video.prototype.handleTimeUpdateEvent = function(_this) {
    return (function(_this) {
      return function() {

        var percent = Math.floor(_this.elementRef.currentTime / _this.elementRef.duration * 100);
        var roundedPercent = Math.round(percent / _this.trackStops) * _this.trackStops;

        if (roundedPercent % (100 / _this.trackStops) === 0 && this.lastStop !== roundedPercent) {
          _this.gaTrackEvent("", "reached " + roundedPercent + " percent", _this.name);
          this.lastStop = roundedPercent;
        }
      }

    })(_this)
  }

  Video.prototype.handlePlayEvent = function(_this) {
    return (function(_this) {
      return function() {
        _this.gaTrackEvent("", "played", _this.name);
      }
    })(_this)
  }

  Video.prototype.handleEndedEvent = function(_this) {
    return (function(_this) {
      return function() {
        _this.gaTrackEvent("", "ended", _this.name);
      }
    })(_this)
  }

  Video.prototype.gaTrackEvent = function(category, action, label) {

    category = category || "video";
    action = action || "unknown action";
    label = label || "unknown label";

    window.ga("send", "event", category, action, label);
    
  };


  $.fn.ot_gvt = function() {
    return this.each(function() {
      var videos = [];
      videos.push(new Video(this, this.id));
    });
  };

})(window, jQuery);
